// Use the count operator to count the total number of fruits on sale.

db.fruits.aggregate([
	{$match: {onSale:true}},
	{fruitsOnSale:{$count: "$onSale"}}
]);


// Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.count([
	{$match: {stock: {$gt: 20}},
	{enoughStock:{$count: "$stock"}}
]);





// Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
	{$group:{_id:"$supplier_id", avg_price:{$avg: "$price"}}}
	]);


// Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate([
	{$group:{_id:"$supplier_id", max_price:{$max: "$price"}}}
	]);

// Use the min operator to get the lowest price of a fruit per supplier.
db.fruits.aggregate([
	{$group:{_id:"$supplier_id", min_price:{$min: "$price"}}}
	]);




