 db.fruits.insertMany({
        name: "apple",
        color: "red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: [ "Philippines", "US" ]

        },
        
     {
         name: "banana",
         color: "yellow",
         stock: 15, 
         price: 20,
         supplier_id: 2,
         onSale: true,
         origin: [ "Philippines", "Ecuador" ]
         },
         
      {
          name: "kiwi",
          color: "green",
          stock: 25, 
          price: 50,
          supplier_id: 1,
          onSale: true,
          origin: [ "US", "China" ]
          },
          
      {
           name:"mango",
           color: "green",
           stock: 10,
           price: 120,
           supplier_id: 2,
           onSale: false,
           origin: [ "Philippines", "India" ]
          }
)




 //SECTION - MongoDB Aggregation
/*
    -used to generate manipulated data and perform operations to create filtered results that help in analyzing data

    -Compared to CRUD operations on our data from previous sessions , aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to creat frontend application

 */

 // SECTION - USING AGGREGATE METHOD
/*
    $match
        - used to pass the documents that meet the specified conditions to the next pipepline/aggregation process

        -pipelines/aggregation process is the series of aggregation methods should the dev/client wabts ti use two or more 
*/

db.fruits.aggregate([
    {$match:{onSale:true}}
]);

/*
    $group
        -used to group elements together and field-value pairs used the data from the grouped elements

*/

db.fruits.aggregate([
        {$group:{_id:"$supplier_id", total:{$sum:"$stock"}}}
    ]);

/*
    using both $match and $group along with aggregation will find the products that are on sale and will group the total amount of stocks for all suppliers found(this will be done in order)
*/

db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group:{_id:"$supplier_id", total:{$sum:"$stock"}}}
    ]);


// FIELD projections with aggregatio
/*
    $project
        - this can be used when aggregating data to include/exclude fields from the returned result
*/


db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group:{_id:"$supplier_id", total:{$sum:"$stock"}}},
        {$project:{_id:0}}
    ]);

/*
    $sort
    -used to change the order of aggregated results
    -providing -1 as the value will result to MongoDB sorting the documents in reversed order
*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group:{_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort:{total: -1}}

]);


db.fruits.aggregate([
    {$group: {_id: "$origin", kinds: {$sum: 1}}}
]);

/*
    $unwind
    - used to deconstruct an array field from a collection/field arry value  to ouput a result  for each element
*/
db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin", kinds: {$sum: 1}}}
    ]);

// SECTION - Schema Design
/*
    -schema design/data modelling is an important feature creating database 
    - MongoDB documents can be categorixed into normalized or de-normalize embedded data
*/


var owner = ObjectId();


db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "0912345789"
    });


db.suppliers.insert({
    name: "Jane Smith",
    contact: "09987654321",
    owner_id: owner
});

// de-normalized data scheme design with one-to-few relationship

db.suppliers.insert({
    name: "DEF fruits",
    contact: "09123456789",
    address:[
        {street: "123 San Jose St.", city:"Manila"},
        {street: "367 Gil Puyat", city:"Makati"}
    
    ]
});

/*
    Both data structures are common practices but each of them has its own pros and cons
    Difference between Normalized and De-normalized
    NORMALIZED
        - it makes it easier to read information because separate documents can be retrieved

        - in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.

        -this approach is useful for data structures where pieces of information are common operated 

    DE-NORMALIZED
        - it makes it easier to query documentsand has a faster perfomance bacause only one query needs to be done in order to retrieve the document/s


        -if the data structure becomes too complex and long it makes it more difficult to manipulate and access information


        -this approach is applicable for data sturctures where pieces of information are commonl retrieved and rarely oparated on/changed


*/

var supplier = ObjectId();
var branch = ObjectId();


db.suppliers.insert({
    _id: supplier,
    name: "ZXY hardware",
    contact: "0912345432",
    branches: [
        branch
    ]
    });

db.branches.insert({
    _id:ObjectId("62d5612d144b3427e3dd11f3")
    name:"TUV",
    address: "123 Arcadio Santos St.",
    city:"Paranaque",
    supplier_id:ObjectId("62d5612d144b3427e3dd11f2")

});








// 